<?php

namespace App\Http\Controllers;

use App\Models\Dosen;
use App\Models\Pendidikan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class PendidikanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pendidikan = Pendidikan::all();

        return view('pendidikan.index', [
            'title' => 'Riwayat Pendidikan',
            'pendidikan' => $pendidikan
        ]);
    }

    /**
     * Show the form for creating a new resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function create($dosen_NIP = 0)
    {
        return view('pendidikan.create', [
            'title' => 'Create Riwayat Pendidikan',
            'pendidikan' =>  Pendidikan::where('dosen_NIP', $dosen_NIP)->first(),
            'dosen_NIP' => $dosen_NIP
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['id'] = '';
        Pendidikan::create($data);
        return $this->show($data['dosen_NIP'])->with('success', 'Data has been created');
    }

    /**
     * Display the specified resource.
     *
    //  * @param  \App\Models\Pendidikan  $pendidikan
    //  * @return \Illuminate\Http\Response
     */
    public function show($dosen_NIP)
    {
        $pendidikan = Pendidikan::where('dosen_NIP', $dosen_NIP)->get();
        return view('pendidikan.index', [
            'title' => 'Edit Riwayat Pendidikan',
            'pendidikan' => $pendidikan,
            'dosen_NIP' => $dosen_NIP
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Pendidikan  $pendidikan
     * @return \Illuminate\Http\Response
     */
    public function edit(Pendidikan $pendidikan)
    {
        return view('pendidikan.edit', [
            'title' => 'Edit Riwayat Pendidikan',
            'pendidikan' => $pendidikan
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Pendidikan  $pendidikan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pendidikan $pendidikan)
    {
        $data = $request->except(['_token', '_method']);
        Pendidikan::where('id', $pendidikan['id'])->update($data);
        return $this->show($data['dosen_NIP'])->with('success', 'Data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Pendidikan  $pendidikan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pendidikan $pendidikan)
    {
        Pendidikan::destroy($pendidikan['id']);
        return back()->with('success', 'Data has been deleted');
    }
}
