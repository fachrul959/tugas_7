<?php

namespace App\Http\Controllers;

use App\Models\Matakuliah;
use App\Models\Dosen;
use Illuminate\Http\Request;

class MatakuliahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('matakuliah.index', [
            'title' => 'Mata Kuliah',
            'matakuliah' => Matakuliah::get()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('matakuliah.create', [
            'title' => 'Create Mata Kuliah',
            'dosen' => Dosen::get()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $matakuliah = Matakuliah::create($data);
        $matakuliah->dosen()->sync($data['dosen_id']);
        return redirect('/matakuliah')->with('success', 'Data has been created');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    public function show(Matakuliah $matakuliah)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    public function edit(Matakuliah $matakuliah)
    {
        $nip = array();
        foreach ($matakuliah->dosen as $item) {
            $nip[] = $item->NIP;
        }
        return view('matakuliah.edit', [
            'title' => 'Edit Mata Kuliah',
            'matakuliah' => $matakuliah,
            'dosen' => Dosen::whereNotIn('NIP', $nip)->get()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Matakuliah $matakuliah)
    {
        $dataMK = $request->except(['_token', '_method', 'dosen_id']);
        $dataPivot = $request->all('kode', 'dosen_id');
        Matakuliah::where('kode', $matakuliah['kode'])->update($dataMK);
        $matakuliah->dosen()->attach($dataPivot['dosen_id']);
        return redirect('/matakuliah')->with('success', 'Data has been updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Matakuliah  $matakuliah
     * @return \Illuminate\Http\Response
     */
    public function destroy(Matakuliah $matakuliah)
    {
        Matakuliah::destroy($matakuliah['kode']);
        return redirect('/matakuliah')->with('success', 'Data has been deleted');
    }
}
