-- MySQL dump 10.13  Distrib 5.7.33, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sia
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `dosen`
--

DROP TABLE IF EXISTS `dosen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dosen` (
  `Nama` varchar(50) DEFAULT NULL,
  `NIP` int(30) NOT NULL,
  `Gelar` varchar(30) DEFAULT NULL,
  `id_user` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`NIP`),
  KEY `id_user` (`id_user`),
  CONSTRAINT `dosen_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dosen`
--

LOCK TABLES `dosen` WRITE;
/*!40000 ALTER TABLE `dosen` DISABLE KEYS */;
INSERT INTO `dosen` VALUES ('Agung Setiawan',1101100,'Dr. S.T, M.T.',NULL),('Doddy',1101101,'S.T, M.T, Ph.D.',NULL),('Andi',1101102,'S.T, M.T',NULL),('Rizal',1102111,'S.T, M.T',NULL),('Joko',1102113,'S.T, M.T',NULL);
/*!40000 ALTER TABLE `dosen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dosen_matakuliah`
--

DROP TABLE IF EXISTS `dosen_matakuliah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dosen_matakuliah` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `dosen_id` int(30) DEFAULT NULL,
  `matakuliah_id` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `dosen_matakuliah_dosen_NIP_fk` (`dosen_id`),
  KEY `dosen_matakuliah_matakuliah_kode_fk` (`matakuliah_id`),
  CONSTRAINT `dosen_matakuliah_dosen_NIP_fk` FOREIGN KEY (`dosen_id`) REFERENCES `dosen` (`NIP`) ON DELETE SET NULL,
  CONSTRAINT `dosen_matakuliah_matakuliah_kode_fk` FOREIGN KEY (`matakuliah_id`) REFERENCES `matakuliah` (`kode`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dosen_matakuliah`
--

LOCK TABLES `dosen_matakuliah` WRITE;
/*!40000 ALTER TABLE `dosen_matakuliah` DISABLE KEYS */;
INSERT INTO `dosen_matakuliah` VALUES (1,1102111,'TSK21233'),(2,1102111,'TSK21305'),(3,1101101,'TSK21305'),(4,1101101,'TSK21315'),(5,1101101,'TSK21285'),(8,NULL,'TSK21275'),(9,NULL,'TSK21285'),(10,1102113,'TSK21286'),(11,1102113,'TSK21294'),(12,1102111,'TSK21295'),(13,1102113,'TSK21264'),(14,1101101,'TSK21325'),(28,1101102,'TSK21233'),(29,1101102,'TSK21285'),(30,1101102,'TSK21315'),(32,1101100,'TSK21233'),(33,1101100,'TSK21444'),(34,1101100,'TSK21234'),(35,1101100,'TSK21325');
/*!40000 ALTER TABLE `dosen_matakuliah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kelas`
--

DROP TABLE IF EXISTS `kelas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kelas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nim` bigint(20) DEFAULT NULL,
  `kode_mk` varchar(20) DEFAULT NULL,
  `kode_kelas` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `kelas_mahasiswa_nim_fk` (`nim`),
  KEY `kelas_matakuliah_kode_fk` (`kode_mk`),
  CONSTRAINT `kelas_mahasiswa_nim_fk` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`) ON DELETE SET NULL,
  CONSTRAINT `kelas_matakuliah_kode_fk` FOREIGN KEY (`kode_mk`) REFERENCES `matakuliah` (`kode`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kelas`
--

LOCK TABLES `kelas` WRITE;
/*!40000 ALTER TABLE `kelas` DISABLE KEYS */;
INSERT INTO `kelas` VALUES (1,16220316,'TSK21233','E201'),(2,16220316,'TSK21234','E201'),(3,16220316,'TSK21264','E201'),(4,16220316,'TSK21275','E201'),(5,16220316,'TSK21285','E201'),(6,16220316,'TSK21286','E201'),(7,16220316,'TSK21294','E201'),(8,16220316,'TSK21295','E201'),(9,16220316,'TSK21444','E201'),(10,16220317,'TSK21233','F302'),(11,16220317,'TSK21234','F302'),(12,16220317,'TSK21264','F302'),(13,16220317,'TSK21275','F302'),(14,16220317,'TSK21285','F302'),(15,16220317,'TSK21286','F302'),(16,16220317,'TSK21294','F302'),(17,16220317,'TSK21295','F302'),(18,16220317,'TSK21305','F302'),(19,16220320,'TSK21264','F302'),(20,16220320,'TSK21233','F303'),(21,16220320,'TSK21234','F303'),(22,16220320,'TSK21264','F303'),(23,16220320,'TSK21275','F303');
/*!40000 ALTER TABLE `kelas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kelas_data`
--

DROP TABLE IF EXISTS `kelas_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `kelas_data` (
  `nama_kelas` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kelas_data`
--

LOCK TABLES `kelas_data` WRITE;
/*!40000 ALTER TABLE `kelas_data` DISABLE KEYS */;
INSERT INTO `kelas_data` VALUES ('E201'),('E202'),('E203'),('F301'),('F302'),('F303'),('A101'),('A102');
/*!40000 ALTER TABLE `kelas_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mahasiswa`
--

DROP TABLE IF EXISTS `mahasiswa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mahasiswa` (
  `nim` bigint(20) NOT NULL,
  `nama` text,
  `jk` varchar(10) DEFAULT NULL,
  `tempat_lahir` varchar(50) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `id_user` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`nim`),
  KEY `id` (`id_user`),
  CONSTRAINT `mahasiswa_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mahasiswa`
--

LOCK TABLES `mahasiswa` WRITE;
/*!40000 ALTER TABLE `mahasiswa` DISABLE KEYS */;
INSERT INTO `mahasiswa` VALUES (16220316,'Arif','Laki-laki','Jakarta','2000-07-13',2),(16220317,'Budi Ramadhan','Laki-laki','Payakumbuh','1998-05-05',3),(16220320,'Fadilah Amaliyanisa','Perempuan','Pekanbaru','1999-02-11',4);
/*!40000 ALTER TABLE `mahasiswa` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `matakuliah`
--

DROP TABLE IF EXISTS `matakuliah`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `matakuliah` (
  `kode` varchar(20) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `jumlah_sks` int(10) DEFAULT NULL,
  PRIMARY KEY (`kode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `matakuliah`
--

LOCK TABLES `matakuliah` WRITE;
/*!40000 ALTER TABLE `matakuliah` DISABLE KEYS */;
INSERT INTO `matakuliah` VALUES ('TSK21233','Jaringan Komputer',3),('TSK21234','Arsitektur Komputer',2),('TSK21264','Kriptografi',3),('TSK21275','Metode Numerik',2),('TSK21285','Probabilitas dan Statistik',2),('TSK21286','Multimedia',2),('TSK21294','Sistem Basis Data',2),('TSK21295','Rekayasa Perangkat Lunak',2),('TSK21305','Robotika',3),('TSK21315','Sistem Digital Lanjut',2),('TSK21325','Sistem Tertanam',2),('TSK21444','Praktikum Robotika',1);
/*!40000 ALTER TABLE `matakuliah` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2019_12_14_000001_create_personal_access_tokens_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `riwayatpendidikan`
--

DROP TABLE IF EXISTS `riwayatpendidikan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `riwayatpendidikan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `strata` varchar(10) DEFAULT NULL,
  `jurusan` varchar(30) DEFAULT NULL,
  `sekolah` varchar(50) DEFAULT NULL,
  `tahun_mulai` year(4) DEFAULT NULL,
  `tahun_selesai` year(4) DEFAULT NULL,
  `dosen_NIP` int(30) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `fk_1` (`dosen_NIP`),
  CONSTRAINT `fk_1` FOREIGN KEY (`dosen_NIP`) REFERENCES `dosen` (`NIP`) ON DELETE SET NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `riwayatpendidikan`
--

LOCK TABLES `riwayatpendidikan` WRITE;
/*!40000 ALTER TABLE `riwayatpendidikan` DISABLE KEYS */;
INSERT INTO `riwayatpendidikan` VALUES (1,'Strata 1','Teknik Informatika','STEI ITB',1996,2000,NULL),(2,'Strata 2','Teknik Mesin','Universitar Gajah Mada',2001,2003,NULL),(3,'Strata 3','Teknik Elektro','Universitas Gadjah Mada',2004,2007,1102113),(9,'Strata 1','Teknik Mesin','FTMD ITB',1996,2000,1102111),(11,'Strata 1','Teknik Mesin','FTMD ITB',1996,2000,1102111),(12,'Strata 1','Teknik Informatika','ITB',2001,2005,1101100),(13,'Strata 2','Teknik Informatika','ITB',2006,2008,1101100),(14,'Strata 1','Teknik Mesin','ITB',2001,2005,1101101),(15,'Strata 2','Teknik Mesin','ITB',2006,2008,1101101),(16,'Strata 1','Teknik Elektro','ITB',1996,2000,1101102);
/*!40000 ALTER TABLE `riwayatpendidikan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Admin','$2y$10$ChNDXAhCLibI37peP2IpD.ca6Hnf5lzS.fWUMf2qlAEyFQiOGR.ZO',1,'2022-02-26 20:31:24','2022-02-26 20:31:24','admin@gmail.com'),(2,'Arif','$2y$10$SZQbw2tj9ZT.HO/DwO6Hi.vP0V0QoZYr6qmLvvaIwm.cQo8IuOfKm',0,'2022-03-07 19:39:00','2022-03-07 19:39:00','arif@gmail.com'),(3,'Budi Ramadhan','$2y$10$PwfP/3QeANLJVsJgru4kXuXHbIwdgwJ9Mj9r9xj/31tn4iOtWyqQe',0,'2022-04-22 06:28:10','2022-04-22 06:28:10','budi@sia.com'),(4,'Fadilah Amaliyanisa','$2y$10$b/864yq.u9H/R.eSKlxqlOFKXJTJ4nXa2IaiJDN6gWLlGtPSrwBfC',0,'2022-04-22 07:50:17','2022-04-22 07:50:17','dila@sia.com');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-12 18:37:19
