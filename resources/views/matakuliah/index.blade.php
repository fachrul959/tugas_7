@extends('layouts.main')
@section('container')
<div
     class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Mata Kuliah</h1>
</div>

@if (session()->has('success'))
<div class="alert alert-success alert-dismissible fade show col-lg-8" role="alert">
    {{ session('success') }}
    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
</div>
@endif
<div class="table-responsive col-lg-8">
    <a href="/matakuliah/create" class="btn btn-primary mb-3">Create New Data</a>
    <table class="table table-striped table-sm">
        <thead>
            <tr>
                <th scope="col">Kode</th>
                <th scope="col">Nama</th>
                <th scope="col">Nama Dosen</th>
                <th class="text-center" scope="col">Jumlah SKS</th>
                <th class="text-center" scope="col">Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($matakuliah as $item)
            <tr>
                <td>{{ $item->kode }}</td>
                <td>{{ $item->nama }}</td>
                <td>
                    <ul style="list-style-type: none;padding-left: 0px;">
                        @foreach ($item->dosen as $d)
                        <li>{{ $d->Nama }}</li>
                        @endforeach
                    </ul>
                </td>
                <td class="text-center">{{ $item->jumlah_sks }}</td>
                <td class="text-center">
                    <a href="/matakuliah/{{ $item->kode }}/edit" class="badge bg-warning"><span
                              data-feather="edit"></span></a>
                    <form method="post" action="/matakuliah/{{ $item->kode }}" class="d-inline">
                        @method('delete')
                        @csrf
                        <button class="badge bg-danger border-0" onclick="return confirm('Are you sure?')"><span
                                  data-feather="trash"></span></button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
