@extends('layouts.main')

@section('container')
<div
     class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
    <h1 class="h2">Edit Data</h1>
</div>
<div class="col-lg-8">
    <form method="POST" action="/pendidikan/{{ $pendidikan->id }}">
        @method('put')
        @csrf
        <div class="mb-3">
            <label for="nip" class="form-label">NIP Dosen</label>
            <input type="text" name="nip_dosen" class="form-control" id="nip" value="{{ $pendidikan->dosen_NIP }}">
        </div>
        <div class="mb-3">
            <label for="strata" class="form-label">Strata</label>
            <input type="text" name="strata" class="form-control" id="strata"
                   value="{{ $pendidikan->strata }}">
        </div>
        <div class="mb-3">
            <label for="jurusan" class="form-label">Jurusan</label>
            <input type="text" name="jurusan" class="form-control" id="jurusan"
                   value="{{ $pendidikan->jurusan }}">
        </div>
        <div class="mb-3">
            <label for="sekolah" class="form-label">Sekolah</label>
            <input type="text" name="sekolah" class="form-control" id="sekolah"
                   value="{{ $pendidikan->sekolah }}">
        </div>
        <div class="mb-3">
            <label for="tahun_mulai" class="form-label">Tahun Mulai</label>
            <input type="text" name="tahun_mulai" class="form-control" id="tahun_mulai"
                   value="{{ $pendidikan->tahun_mulai }}">
        </div>
        <div class="mb-3">
            <label for="tahun_selesai" class="form-label">Tahun Selesai</label>
            <input type="text" name="tahun_selesai" class="form-control" id="tahun_selesai"
                   value="{{ $pendidikan->tahun_selesai }}">
        </div>
        <button type="submit" class="btn btn-primary">Update Data</button>
    </form>
</div>
@endsection
